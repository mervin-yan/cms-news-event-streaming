import {
  EventStoreDBClient,
  FORWARDS,
  START,
  Direction,
  ReadRevision,
  ResolvedEvent,
  AppendResult,
  EventData,
} from '@eventstore/db-client';

import { Logger } from '@nestjs/common';

export class EventStore {
  client: EventStoreDBClient;

  private logger: Logger = new Logger(this.constructor.name);

  constructor(connectionString: string) {
    this.client = EventStoreDBClient.connectionString(connectionString);
  }

  public async appendToStream(streamId: string, events: EventData | EventData[]): Promise<AppendResult> {
    const result = await this.client.appendToStream(streamId, events);
    // this.logger.log(result);
    return result;
  }

  public async readStream(streamId: string, direction: Direction = FORWARDS, fromRevision: ReadRevision = START, maxCount: number = 1024): Promise<ResolvedEvent<any>[]> {
    const events = await this.client.readStream(streamId, {
      direction: direction,
      fromRevision: fromRevision,
      maxCount: maxCount
    });
    this.logger.log(events);
    return events;
  }
}