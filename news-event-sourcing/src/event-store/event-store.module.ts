import { Global, Module, DynamicModule } from '@nestjs/common';
import { EventStore } from './event-store.class';

export interface EventStoreModuleOptions {
  connectionString: string;
}

export interface EventStoreModuleAsyncOptions {
  useFactory: (...args: any[]) => Promise<any> | any;
  inject?: any[];
}

@Global()
@Module({
  providers: [EventStore],
  exports: [EventStore],
})
export class EventStoreModule {
  static forRoot(
    connectionString: string,
  ): DynamicModule {
    return {
      module: EventStoreModule,
      providers: [
        {
          provide: EventStore,
          useFactory: () => {
            return new EventStore(connectionString);
          },
        },
      ],
      exports: [EventStore],
    };
  }

  static forRootAsync(options: EventStoreModuleAsyncOptions): DynamicModule {
    return {
      module: EventStoreModule,
      providers: [
        {
          provide: EventStore,
          useFactory: async (...args) => {
            const { connectionString } = await options.useFactory(
              ...args,
            );
            return new EventStore(connectionString);
          },
          inject: options.inject,
        },
      ],
      exports: [EventStore],
    };
  }
}
