import { Module } from '@nestjs/common';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { ConfigModule, ConfigService } from '@nestjs/config';

import configuration from './config/configuration';
import { EventStoreModule } from './event-store/event-store.module';
import { ContentfulModule } from './contentful/contentful.module';
import { HealthModule } from './health/health.module';

@Module({
  imports: [
    ConfigModule.forRoot({
      load: [configuration],
      isGlobal: true,
    }),
    EventStoreModule.forRootAsync({
      useFactory: async (config: ConfigService) => {
        return {
          connectionString: config.get('eventstore.connectionString')
        };
      },
      inject: [ConfigService]
    }),
    ContentfulModule.forRootAsync({
      useFactory: async (config: ConfigService) => {
        return {
          spaceId: config.get('contentful.spaceId'),
          accessToken: config.get('contentful.accessToken'),
          environmentId: config.get('contentful.environmentId')
        };
      },
      inject: [ConfigService]
    }),
    HealthModule,
  ],
  controllers: [AppController],
  providers: [AppService],
})
export class AppModule {}