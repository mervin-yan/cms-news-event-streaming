import { Controller, Get, Post, Req } from '@nestjs/common';
import { AppService } from './app.service';
import { EventStore } from './event-store/event-store.class';
import { Request } from 'express';

import { Logger } from '@nestjs/common';
import { jsonEvent } from '@eventstore/db-client';
import { v4 as uuidv4 } from 'uuid';


@Controller()
export class AppController {
  private logger: Logger = new Logger(this.constructor.name);

  constructor(private readonly appService: AppService, private readonly eventStore: EventStore) { }

  @Get()
  getHello(): string {
    return this.appService.getHello();
  }

  @Post('webhooks')
  async handleWebhook(@Req() request: Request) {
    const streamId = request.body['sys']['type'] === 'Entry' && request.body['sys']['contentType'] ? `${request.body['sys']['contentType']['sys']['id']}-${request.body['sys']['id']}` : `asset-${request.body['sys']['id']}`;
    const topic = String(request.headers['x-contentful-topic']);
    const lastDotPosition = topic.lastIndexOf(".");
    const eventType = topic.substring(lastDotPosition + 1, topic.length);
    this.logger.log(`${streamId}: ${eventType}`);
    const event = jsonEvent({
      id: uuidv4(),
      type: eventType,
      data: {
        headers: { ...request.headers },
        body: { ...request.body }
      },
    });
    try {
      const result = await this.eventStore.appendToStream(`contentful-${streamId}`, event);
    } catch (error) {
      this.logger.error(error)
    }


  }
}
