import { Global, Module, DynamicModule } from '@nestjs/common';
import { Contentful } from './contentful.class';

export interface ContenfulModuleOptions {
  spaceId: string;
  accessToken: string;
  environmentId: string;
}

export interface ContentfulModuleAsyncOptions {
  useFactory: (...args: any[]) => Promise<any> | any;
  inject?: any[];
}

@Global()
@Module({
  providers: [Contentful],
  exports: [Contentful],
})
export class ContentfulModule {
  static forRoot(
    spaceId: string,
    accessToken: string,
    environmentId: string,
  ): DynamicModule {
    return {
      module: ContentfulModule,
      providers: [
        {
          provide: Contentful,
          useFactory: () => {
            return new Contentful(spaceId, accessToken, environmentId);
          },
        },
      ],
      exports: [Contentful],
    };
  }

  static forRootAsync(options: ContentfulModuleAsyncOptions): DynamicModule {
    return {
      module: ContentfulModule,
      providers: [
        {
          provide: Contentful,
          useFactory: async (...args) => {
            const { spaceId, accessToken, environmentId } = await options.useFactory(
              ...args,
            );
            return new Contentful(spaceId, accessToken, environmentId);
          },
          inject: options.inject,
        },
      ],
      exports: [Contentful],
    };
  }
}
