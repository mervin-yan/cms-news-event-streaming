import * as contentful from 'contentful';

import { Logger } from '@nestjs/common';

export class Contentful {
  client: contentful.ContentfulClientApi

  private logger: Logger = new Logger(this.constructor.name);

  constructor(spaceId: string, accessToken: string, envorinmentId: string) {
    this.client = contentful.createClient({
      space: spaceId,
      accessToken: accessToken,
      environment: envorinmentId
    });
    
    this.logger.log(this.client);
  }
}