import { NestFactory } from '@nestjs/core';
import { AppModule } from './app.module';
import * as bodyParser from 'body-parser';

async function bootstrap() {
  const app = await NestFactory.create(AppModule, {
    bodyParser: true
  });
  app.use(bodyParser.json({ type: (req: any) => req.get('Content-Type') === 'application/vnd.contentful.management.v1+json', strict: false }));
  await app.listen(3030);
}
bootstrap();