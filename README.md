
# How to add local webhook to contentful

1. Download and unzip ngrok
2. Connect to your account
https://dashboard.ngrok.com/get-started/setup
`./ngrok authtoken <your_auth_token>`
3. Fire it up
`./ngrok http 3030`
4. Copy the https forwarding url
![Ngrok](./docs/ngrok.png)
5. Login to contentful and add a new webhook with the copied url from step4

# How to start eventstore
`sudo systemctl start eventstore`

`sudo eventstored --insecure --run-projections=all startstandardprojections=true --enable-atom-pub-over-http=true --enable-external-tcp`

# Monitoring eventstore with prometheus and grafana
https://www.eventstore.com/blog/monitoring-your-eventstoredb-with-prometheus-and-grafana

get all keys from redis
redis-cli --scan --pattern '*'

42 target regions

In-Memory Tree for Target Region data for fast processing

Naads Regions
![DataAccessHub Profile ](./docs/data-access-hub-profile.png)

## Get Region Ids by placeCode
### Get Naads Region Codes based on place code
### Find unique list of Region Id based on the returned Naads Region Codes by traversalling through TargetRegionGraph 

## Get Region Ids by regionId
### Find parent regions for the regionId by traversalling through TargetRegionGraph 

## Endpoints
### NewsArtcles
by slug: 
`/api/news-article/url/:slug?preview={preview}`
if preview is true then
fetch the news article that status is either save or auto_save
else
fetch the news article that status is publish

resolve news article references
resolve news body videos
for each embeded video iframe "iframe.embedly-embed[src*='brightcove']"
get video account_id and video_id
get video sponsorship for each video according to account_id and video_id

getVideoById: https://cms-video-api:5002/api/${locale}/video/brightcove/${accountId}/${videoId}

get first `_partnership/' tag from response

attach the sponsor to the `iframe.embedly-embed[src*='brightcove']` tag as value of the attribute `data-sponsorship`


by id: 
`/api/news-article/:id?preview={preview}`

if preview is true then
fetch the news article that status is either save or auto_save
else
fetch the news article that status is publish

resolve news article references


by slugs: 

`/api/news-articles?slugs={slugs}`
![Get News Artciles by Slugs](./docs/get-news-articles-by-slugs.png)



editorial-lineup
for each lineup item
if it is video, resolve video
![Resolve Video](./docs/resolve-brightcove-video.png)
if it is article, resolve article

### NewsList
`/api/news-list/:locale/line-up-region-with-latest/:listType/:quantity?/:fields?preview={preview}&regioncode={regioncode}&thirdparty={thirdparty}&resolvevideos={recresolvevideosent}`
getEditorialLineupWithLatest
if showThirdPartyArticles flag is false then set doVideoResolution as false

if there is no regionId then getLatestArtciles based on updatedAt (effective_date ?) timestamp
![Get Latest Articles Conditions](./docs/get-latest-articles-conditions.png)
for each article, resolve article

if there is regionId then getEditorialLineup based on regionId
get editorialLinup based on market and listType
resolve editorialineup item
get region ids by placecode or regionId
filter lineup items based on regions
filter lineup items by priority
if listType is headlineText then return a list of object 
`
{
  message: lineupItem.headline,
  CTA: lineupItem.url,
  priority: lineupItem.priority,
}
`
![Headline Text Lineup](./docs/editorial-lineup-headline-text.png)

`/api/news-list/:locale/latest/:quantity?/:fields?preview={preview}&thirdparty={thirdparty}&safefordistributiononly={safefordistributiononly}&recent={recent}`
![Latest News Articles](./docs/news-list-latest-articles.png)

`/api/news-list/:locale/category/:category/:quantity?preview={preview}`

getNewsArticlesByCategory
![News List by Category](./docs/news-list-category.png)

`/api/news-list/:locale/keyword/:keyword/:quantity?preview={preview}`

getNewsArticlesByKeyword
![News List by Keyword](./docs/news-list-keyword.png)

`/api/news-list/:locale/author/:author/:quantity?preview={preview}&category={category}`

getNewsArticlesByAuthor
![News List by Author](./docs/news-list-author.png)

`/api/news-list/:locale/line-up/:listType/:placeCode?preview={preview}&priority={priority}`

![News List Lineup](./docs/news-list-lineup.png)
getTrendingNowWithLatest
![TrendingNow with Latest](./docs/get-trendingnow-with-latest.png)


### Views
`Editorial Lineups`
```
{
  locale: string,
  listType: string,
  items: [

  ]
}
```

if lineup-item's region is in region-id list then return this lineup-item

### ContentCategories
`/api/content-categories/locale/:locale/url/:url?preview={preview}`
find market by locale and status 'publish'
if preview is false then find one content-category by status 'publish', url  and marketId
if preview is true then find one content-category by url  and marketId

`/api/content-categories/locale/:locale?preview={preview}`
find market by locale and status 'publish'
if preview is false then find a list of content-category by status 'publish', url,  marketId, and displayInNavigation with true
if preview is true then find a list of content-category by url, marketId, and displayInNavigation with true

### ContentKeywords
`/api/content-keywords/locale/:locale/key/:key?preview={preview}`
find market by locale and status 'publish'
if preview is false then find one content-keyword by status 'publish', key  and marketId
if preview is true then find one content-keyword by key  and marketId


### promos
`/api/promo/:locale/:slot`


## news-event-sourcing

### event-types
DELETE, ARCHIVE: delete data from mongodb
UNPUBLISH: update model's status accordingly

## news-event-processing

### eligbility
contentType is 'newsarticles" and safeForDistribution is true (and url and market exists

get locale from news article 

delete news article when the event type is one of the following: DELETE, ARCHIVE, UNPUBLISH
send delete request: http://apple-news-service:8080/api/article/{locale}/{apple-news-id}

publish news article when the event type is PUBLISH
send post request when apple-news-id not exist: http://apple-news-service:8080/api/article/{locale}/{news-article-url}

send put request when apple-news-id exists: http://apple-news-service:8080/api/article/{locale}/{news-article-url}

### internal hook info
![Update Apple News Article](./docs/update-apple-news-article.png)

![Update Apple News Internal Hook Info](./docs/update-apple-news-internal-hook-info.png)

### map fields
![Map fields](./docs/map-fields.png)

### update data

create a copy of news-article which id has prefix `preview_` when the event type is AUTO_SAVE and SAVE

delete the preview copy when the event type is PUBLISH

### resolve asset when the event-type is PUBLISH
resolve image asset by calling contentful api

replace "images.ctfassets.net" with "images.twnmm.com"
replace "assets.ctfassets.net" with "images.twnmm.com"

### resolve embeded when the event-type is PUBLISH
resolve `.embedly-card` links from news-body
Use `cheerio` to parse news-body content and find all elements where class is `.embedly-card` 
get url from `href` attribute and call embedly api
from the returned embedly api result: 
if provider_name is twitter and description and title exsits then use the cheerio again to parse the embeded html
prepend `<p>${embedData.description}</p>` for `twitter-tweet`
override text with `embedData.title`

![Resolve embedly twitter tweet](./docs/resolve-embedly-twitter-tweet.png)

### unused functions:
buildEntity, generateEntity in `service.ts`
