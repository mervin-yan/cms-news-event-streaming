import {
  EventStoreDBClient,
  FORWARDS,
  START,
  Direction,
  ReadRevision,
  ResolvedEvent,
  AppendResult,
  EventData,
  END,
} from '@eventstore/db-client';

import { Logger } from '@nestjs/common';
import { ContentfulClientApi } from 'contentful';

export class EventStore {
  client: EventStoreDBClient;

  private logger: Logger = new Logger(this.constructor.name);

  constructor(connectionString: string) {
    this.client = EventStoreDBClient.connectionString(connectionString);
  }


  public async readStream(streamId: string, direction: Direction = FORWARDS, fromRevision: ReadRevision = START, maxCount: number = 1024): Promise<ResolvedEvent<any>[]> {
    const events = await this.client.readStream(streamId, {
      direction: direction,
      fromRevision: fromRevision,
      maxCount: maxCount
    });
    this.logger.log(events);
    return events;
  }

  public async subscribeToStream(streamId: string, handleEvent: (resolvedEvent: ResolvedEvent) => void) {
    const subscription = this.client
      .subscribeToStream(streamId, { fromRevision: START })
      .on("data", handleEvent);
  }

}