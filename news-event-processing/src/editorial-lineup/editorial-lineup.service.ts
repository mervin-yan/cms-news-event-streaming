import { Injectable } from '@nestjs/common';

import { Logger } from '@nestjs/common';
import { EventStore } from '../event-store/event-store.class';
import { Contentful } from '../contentful/contentful.class';
import { ResolvedEvent } from '@eventstore/db-client';
import { ContentfulClientApi } from 'contentful';

@Injectable()
export class EditorialLineupService {

  private logger: Logger = new Logger(this.constructor.name);

  constructor(private readonly eventStore: EventStore, private readonly contentful: Contentful) { 

  }

  start(): void {
    this.eventStore.subscribeToStream('editorialLineups-4xNaboSEKI6o26imKeGuOS', this.handleEvent);
  }

  private async handleEvent(resolvedEvent: ResolvedEvent) {
    const eventBody = resolvedEvent.event.data['body'];
    console.log(eventBody);
    for (const [key, value] of Object.entries(eventBody.fields)) {
      console.log(value)
      if (!value['en-CA']['sys']) {
        console.log(value['en-CA']);
      } else {
        if (value['en-CA']['sys']?.['type'] === 'Link') {
          if (value['en-CA']['sys']['linkType'] === 'Asset') {
            const asset = await this.contentful.client.getAsset(value['en-CA']['sys']['id']);
            console.log(asset);
          } else if (value['en-CA']['sys']['linkType'] === 'Entry') {
            const entry = await this.contentful.client.getEntry(value['en-CA']['sys']['id']);
            console.log(entry);
          }
        }
      }
    }
    // console.log(resolvedEvent)
  }
}
