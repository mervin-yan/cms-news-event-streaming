import { Test, TestingModule } from '@nestjs/testing';
import { EditorialLineupService } from './editorial-lineup.service';

describe('EditorialLineupService', () => {
  let service: EditorialLineupService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [EditorialLineupService],
    }).compile();

    service = module.get<EditorialLineupService>(EditorialLineupService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});
