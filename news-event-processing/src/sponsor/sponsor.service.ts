import { END, ResolvedEvent, streamNameFilter } from '@eventstore/db-client';
import { CACHE_MANAGER, Inject, Injectable, Logger } from '@nestjs/common';
import { fetchAsset } from 'src/common/fetch-asset';
import { fetchMarket } from 'src/common/fetch-market';
import { Sponsor } from 'src/common/schema';
import { EventStore } from 'src/event-store/event-store.class';
import { Cache } from 'cache-manager';

@Injectable()
export class SponsorService {
  private logger: Logger = new Logger(this.constructor.name);

  constructor(private readonly eventStore: EventStore, @Inject(CACHE_MANAGER) private cacheManager: Cache) {

  }

  async start(): Promise<void> {
    const sponsorStreamPrefix = "sponsor-";
    const filter = streamNameFilter({ prefixes: [sponsorStreamPrefix] });

    const subscription = this.eventStore.client.subscribeToAll({ filter: filter, fromPosition: END });

    subscription.on('data', async (resolvedEvent: ResolvedEvent) => {

      this.logger.log(`Received event ${resolvedEvent.event.revision}@${resolvedEvent.event.streamId}`);
      const eventBody = resolvedEvent.event.data['body'];

      const id = eventBody['sys']['id'];

      const name = eventBody['fields']['name']['en-CA'];

      const adTag = eventBody['fields']['adTag']['en-CA'];
      const linkText = eventBody['fields']['linkText']['en-CA'];
      const linkUrl = eventBody['fields']['linkUrl']['en-CA'];
      const linkTrackingText = eventBody['fields']['linkTackingText']['en-CA'];

      const image = await fetchAsset(eventBody['fields']['image']['en-CA']['sys']['id']);

      const sponsor: Sponsor = {
        name: name,
        adTag: adTag,
        linkText: linkText,
        linkUrl: linkUrl,
        linkTrackingText: linkTrackingText,
        image: image
      };
      
      const markets = [];

      if (Array.isArray(eventBody['fields']['market']['en-CA'])) {
        for (const market of eventBody['fields']['market']['en-CA']) {
          const marketId = market['sys']['id'];
          markets.push(await fetchMarket(marketId));
        }
      }

      for (const market of markets) {
        const key = `sponsor-${market.locale.toLocaleLowerCase()}-${id}`;
        await this.cacheManager.set(key, sponsor, { ttl: 10 * 365 * 24 * 60 * 60 });
        this.logger.log(`set cache item: ${key}`);
      }
    });

  }
}

