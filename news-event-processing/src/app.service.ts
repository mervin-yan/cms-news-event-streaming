import { CACHE_MANAGER, Inject, Injectable } from '@nestjs/common';

import { Cache } from 'cache-manager';

import { Logger } from '@nestjs/common';
import { EventStore } from './event-store/event-store.class';
import { ResolvedEvent, streamNameFilter, END } from '@eventstore/db-client';

import axios from 'axios';
import { Image } from './schema/image.schema';
import { Model } from 'mongoose';
import { InjectModel } from '@nestjs/mongoose';
import { LineupArticleRegion, LineupArticleRegionDocument } from './schema/lineup-article-region.schema';
import { NewsArticle, NewsArticleDocument } from './schema/news-article.schema';
import { Author, AuthorDocument } from './schema/author.schema';
import { EditorialLineup as EditorialLineup1, EditorialLineupDocument } from './schema/editorial-lineup.schema';

interface Market {
  id: string;
  name: string;
  locale: string;
}

interface Article {
  name: string;
  headline: string;
  summary: string;
  url: string;
  thumbnail: {
    title: string;
    description?: string;
    file: {
      url: string;
      fileName: string;
      contentType: string;
    }
  }
}

interface EditorialLineup {
  id: string;
  name: string;
  listType: string;
  market: Market;
  articles: Article[];
}

@Injectable()
export class AppService {
  private logger: Logger = new Logger(this.constructor.name);

  constructor(private readonly eventStore: EventStore,
    @InjectModel(Author.name) private authorModel: Model<AuthorDocument>,
    @InjectModel(NewsArticle.name) private newsArticleModel: Model<NewsArticleDocument>,
    @InjectModel(LineupArticleRegion.name) private lineupArticleRegionModel: Model<LineupArticleRegionDocument>,
    @InjectModel(EditorialLineup1.name) private editorialLineupModel: Model<EditorialLineupDocument>,
    @Inject(CACHE_MANAGER) private cacheManager: Cache) {

  }

  async parseMarket(marketId: string): Promise<Market> {
    const url = `https://cdn.contentful.com/spaces/${process.env.CONTENTFUL_SPACE_ID}/environments/${process.env.CONTENTFUL_ENVIRONMENT_ID}/entries/${marketId}?access_token=${process.env.CONTENTFUL_ACCESS_TOKEN}`;
    try {
      const response = await axios.get(url);
      return { id: marketId, name: response.data['fields']['name'], locale: response.data['fields']['locale'] };
    } catch (error) {
      console.error(error);
    }
  }

  async parseArticle(lineupArticleRegionId: string): Promise<Article> {
    try {
      const lineupArticleRegionUrl = `https://cdn.contentful.com/spaces/${process.env.CONTENTFUL_SPACE_ID}/environments/${process.env.CONTENTFUL_ENVIRONMENT_ID}/entries/${lineupArticleRegionId}?access_token=${process.env.CONTENTFUL_ACCESS_TOKEN}`;

      const lineupArticleRegionResponse = await axios.get(lineupArticleRegionUrl);

      const articleOrVideoUrl = `https://cdn.contentful.com/spaces/${process.env.CONTENTFUL_SPACE_ID}/environments/${process.env.CONTENTFUL_ENVIRONMENT_ID}/entries/${lineupArticleRegionResponse.data['fields']['articleOrVideo']['sys']['id']}?access_token=${process.env.CONTENTFUL_ACCESS_TOKEN}`;
      const articleOrVideoRespsonse = await axios.get(articleOrVideoUrl);

      const thumbnailUrl = `https://cdn.contentful.com/spaces/${process.env.CONTENTFUL_SPACE_ID}/environments/${process.env.CONTENTFUL_ENVIRONMENT_ID}/assets/${articleOrVideoRespsonse.data['fields']['thumbnail']['sys']['id']}?access_token=${process.env.CONTENTFUL_ACCESS_TOKEN}`;
      const thumbnailRespsonse = await axios.get(thumbnailUrl);

      return {
        name: articleOrVideoRespsonse.data['fields']['name'],
        headline: articleOrVideoRespsonse.data['fields']['headline'],
        summary: articleOrVideoRespsonse.data['fields']['summary'],
        url: articleOrVideoRespsonse.data['fields']['url'],
        thumbnail: {
          title: thumbnailRespsonse.data.fields['title'],
          description: thumbnailRespsonse.data.fields['description'],
          file: {
            url: thumbnailRespsonse.data.fields['file']['url'],
            fileName: thumbnailRespsonse.data.fields['file']['fileName'],
            contentType: thumbnailRespsonse.data.fields['file']['contentType'],
          }
        }
      }
    } catch (error) {
      console.error(error);
    }
  }

  async parseTargetRegion(lineupArticleRegionId: string): Promise<number[]> {
    try {
      const result = [];
      const lineupArticleRegionUrl = `https://cdn.contentful.com/spaces/${process.env.CONTENTFUL_SPACE_ID}/environments/${process.env.CONTENTFUL_ENVIRONMENT_ID}/entries/${lineupArticleRegionId}?access_token=${process.env.CONTENTFUL_ACCESS_TOKEN}`;

      const lineupArticleRegionResponse = await axios.get(lineupArticleRegionUrl);

      if (lineupArticleRegionResponse.data['fields']['region'] && Array.isArray(lineupArticleRegionResponse.data['fields']['region']['en-CA'])) {
        for (const item of lineupArticleRegionResponse.data['fields']['region']['en-CA']) {
          const regionUrl = `https://cdn.contentful.com/spaces/${process.env.CONTENTFUL_SPACE_ID}/environments/${process.env.CONTENTFUL_ENVIRONMENT_ID}/entries/${item['sys']['id']}?access_token=${process.env.CONTENTFUL_ACCESS_TOKEN}`;
          const regionRepsonse = await axios.get(regionUrl);
          result.push(regionRepsonse.data['fields']['regionId']);
        }
      }

      return result;
    } catch (error) {
      console.error(error);
    }
  }

  async start(): Promise<void> {
    // this.eventStore.subscribeToStream('editorialLineups-4xNaboSEKI6o26imKeGuOS', this.handleEvent);
    const editorialLineupsStreamPrefix = "contentful-";
    const filter = streamNameFilter({ prefixes: [editorialLineupsStreamPrefix] });

    const subscription = this.eventStore.client.subscribeToAll({ filter: filter, fromPosition: END });

    subscription.on('data', async (resolvedEvent: ResolvedEvent) => {
      const result = {};
      this.logger.log(`Received event ${resolvedEvent.event.revision}@${resolvedEvent.event.streamId}`);
      const eventBody = resolvedEvent.event.data['body'];
      const eventType = resolvedEvent.event.type;
      if (eventType === 'publish' && eventBody && eventBody['sys']['contentType']) {
        const id = eventBody['sys']['id'];
        const contentType = eventBody['sys']['contentType']['sys']['id'];

        if (contentType === 'editorialLineups') {
          const name = eventBody['fields']['name']['en-CA'];

          const listType = eventBody['fields']['listType']['en-CA'];

          const martkets = [];

          const marketIds = eventBody['fields']['market'] ? eventBody['fields']['market']['en-CA'].map(item => item['sys']['id']) : [];

          const revision = eventBody['sys']['revision'];
          const createdAt = eventBody['sys']['createdAt'];
          const updatedAt = eventBody['sys']['updatedAt'];

          if (Array.isArray(eventBody['fields']['market']['en-CA'])) {
            for (const market of eventBody['fields']['market']['en-CA']) {
              const marketId = market['sys']['id'];
              martkets.push(await this.parseMarket(marketId));

            }
          }

          const articles = [];

          if (Array.isArray(eventBody['fields']['lineupArticleRegionList']['en-CA'])) {
            for (const lineupArticleRegion of eventBody['fields']['lineupArticleRegionList']['en-CA']) {
              const lineupArticleRegionId = lineupArticleRegion['sys']['id'];

              const article = await this.parseArticle(lineupArticleRegionId);
              const regionIds = await this.parseTargetRegion(lineupArticleRegionId);

              articles.push({ article: article, regionIds: regionIds });
            }
          }

          const lineeupArticleRegionIds = eventBody['fields']['lineupArticleRegionList'] ? eventBody['fields']['lineupArticleRegionList']['en-CA'].map(item => item['sys']['id']) : [];

          // console.log(JSON.stringify(result,null,2))

          const editorialLineup: EditorialLineup = { id: id, name: name, listType: listType, market: { ...martkets[0] }, articles: [...articles] };

          const key = `editorialLineups-${editorialLineup.market.locale.toLocaleLowerCase()}-${editorialLineup.listType.toLocaleLowerCase()}`;
          await this.cacheManager.set(key, editorialLineup, { ttl: 24 * 60 * 60 });
          await this.cacheManager.set(id, editorialLineup, { ttl: 24 * 60 * 60 });

          this.logger.log(`set cache item: ${key}`);

          const data = {
            _id: id,
            name: name,
            listType: listType,
            markets: marketIds,
            lineupArticleRegionList: lineeupArticleRegionIds,
            revision,
            createdAt,
            updatedAt
          };          
          
          try {
            await this.editorialLineupModel.updateOne({ _id: id }, data, { upsert: true, setDefaultsOnInsert: true });
            this.logger.log(`Saved editorialLineup ${id}`);
          } catch (error) {
            this.logger.error(error);
          }

        }

        if (contentType === 'author') {
          const fields = eventBody['fields'];

          const name = fields['name']['en-CA'];
          const jobTitle = fields['jobTitle']['en-CA'];
          const bio = fields['bio']['en-CA'];
          const twitterHandle = fields['twitterHandle']['en-CA'];

          const assetId = fields['image']['en-CA']['sys']['id'];
          // const image = await getAsset(assetId);

          const revision = eventBody['sys']['revision'];
          const createdAt = eventBody['sys']['createdAt'];
          const updatedAt = eventBody['sys']['updatedAt'];

          const data = {
            _id: id,
            name: name,
            jobTitle: jobTitle,
            bio: bio,
            twitterHandle: twitterHandle,
            revision,
            createdAt,
            updatedAt
          }
          // const createdAuthor = new this.authorModel({
          //   _id: id,
          //   name: name,
          //   jobTitle: jobTitle,
          //   bio: bio,
          //   twitterHandle: twitterHandle
          // });
          // createdAuthor.save();

          try {
            await this.authorModel.updateOne({ _id: id }, data, { upsert: true, setDefaultsOnInsert: true });
            this.logger.log(`Saved author ${id}`);
          } catch (error) {
            this.logger.error(error);
          }


        }

        if (contentType === 'newsArticle') {
          const fields = eventBody['fields'];

          const name = fields['name']['en-CA'];
          const headline = fields['headline']['en-CA'];
          const url = fields['url']['en-CA'];
          const summary = fields['summary']['en-CA'];
          const body = fields['body']['en-CA'];

          const author = fields['author']['en-CA']['sys']['id'];

          const thumbnailAssetId = fields['thumbnail']['en-CA']['sys']['id'];
          // const thumbnail = await getAsset(thumbnailAssetId);


          const contentCategories = fields['contentCategories']?.['en-CA'].map(item => item.sys.id);
          const markets = fields['market']?.['en-CA'].map(item => item.sys.id);

          const relatedLinks = fields['relatedLinks']?.['en-CA'].map(item => item.sys.id);
          const contentKeywords = fields['contentKeywords']?.['en-CA'].map(item => item.sys.id);

          const safeForDistribution = fields['safeForDistribution']?.['en-CA'];

          const revision = eventBody['sys']['revision'];
          const createdAt = eventBody['sys']['createdAt'];
          const updatedAt = eventBody['sys']['updatedAt'];

          const data = {
            _id: id,
            name,
            headline,
            url,
            summary,
            body,
            author,
            // thumbnail,
            contentCategories,
            markets,
            relatedLinks,
            contentKeywords,
            safeForDistribution,
            revision,
            createdAt,
            updatedAt
          };

          // const _newsAtricle = new this.newsArticleModel(newsAtricle);
          // _newsAtricle.save();

          try {
            await this.newsArticleModel.updateOne({ _id: id }, data, { upsert: true, setDefaultsOnInsert: true });
            this.logger.log(`Saved newsArticle ${id}`);
          } catch(error) {
            this.logger.error(error);
          }

          const key = `newsArticle-${id}`;
          await this.cacheManager.set(key, data, { ttl: 24 * 60 * 60 });

          this.logger.log(`set cache item: ${key}`);
          // console.log(data)

        }

        if (contentType === 'lineupArticleRegion') {
          const fields = eventBody['fields'];

          const name = fields['name']?.['en-CA'];
          const articleOrVideoId = fields['articleOrVideo']?.['en-CA']['sys']['id'];
          const regionIds = fields['region'] ? fields['region']['en-CA'].map(item => item['sys']['id']) : [];
          const alternateHeadline = fields['alternateHeadline']?.['en-CA'];
          const priority = fields['priority']?.['en-CA'];

          const revision = eventBody['sys']['revision'];
          const createdAt = eventBody['sys']['createdAt'];
          const updatedAt = eventBody['sys']['updatedAt'];

          const data = {
            _id: id,
            name: name,
            region: regionIds,
            article: articleOrVideoId,
            alternateHeadline: alternateHeadline,
            priority: priority,
            revision,
            createdAt,
            updatedAt
          }
          // const createdLineupArticleRegion = new this.lineupArticleRegionModel({
          //   _id: id,
          //   name: name,
          //   region: regionIds,
          //   article: articleOrVideoId,
          //   alternateHeadline: alternateHeadline,
          //   priority: priority
          // });
          try {
            await this.lineupArticleRegionModel.updateOne({ _id: id }, data, { upsert: true, setDefaultsOnInsert: true });
            this.logger.log(`Saved lineupArticleRegion ${id}`);
          } catch (error) {
            this.logger.error(error);
          }
        }
      }



    });

  }


}

const getValue = (fields: object, name: string, locale: string = 'en-CA'): string | number | boolean => {
  return fields[name]['en-CA'];
}

const url = (id: string): string => `https://cdn.contentful.com/spaces/${process.env.CONTENTFUL_SPACE_ID}/environments/${process.env.CONTENTFUL_ENVIRONMENT_ID}/assets/${id}?access_token=${process.env.CONTENTFUL_ACCESS_TOKEN}`;

const getAsset = async (id: string): Promise<Image> => {
  const response = await axios.get(url(id));
  return new Image({
    title: response.data.fields['title'],
    url: response.data.fields['file']['url'],
    fileName: response.data.fields['file']['fileName'],
    contentType: response.data.fields['file']['contentType'],
    size: response.data.fields['file']['details']['size'],
    width: response.data.fields['file']['details']['image']['width'],
    height: response.data.fields['file']['details']['image']['height'],
  });
}