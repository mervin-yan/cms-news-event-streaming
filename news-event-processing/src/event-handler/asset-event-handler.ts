import { ResolvedEvent } from "@eventstore/db-client";
import { Asset } from "src/common/schema";
import { Cache } from 'cache-manager';

const handleEvent = async (resolvedEvent: ResolvedEvent, cacheManager: Cache) => {
  const eventBody = resolvedEvent.event.data['body'];

  const id = eventBody['sys']['id'];
  const key = `asset-${id}`;
  if (resolvedEvent.event.type === 'publish') {
    const title = eventBody['fields']['title']['en-CA'];

    const fileObject = eventBody['fields']['file']['en-CA'];
  
    const asset: Asset = {
      id: id,
      title: title,
      file: fileObject,
    };

    await cacheManager.set(key, asset, { ttl: 10 * 365 * 24 * 60 * 60 });
  } else if (resolvedEvent.event.type === 'delete') {
    await cacheManager.del(key);
  }
}