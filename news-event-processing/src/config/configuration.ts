export default () => ({
  eventstore: {
    connectionString: process.env.EVENT_STORE_CONNECTION_STRING,
  },
  contentful: {
    spaceId: process.env.CONTENTFUL_SPACE_ID,
    accessToken: process.env.CONTENTFUL_ACCESS_TOKEN,
    environmentId: process.env.CONTENTFUL_ENVIRONTMENT_ID
  }
});