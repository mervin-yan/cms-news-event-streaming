import axios from 'axios';
import { Asset } from './schema';

export const fetchAsset = async (id: string): Promise<Asset> => {
  const url = `https://cdn.contentful.com/spaces/${process.env.CONTENTFUL_SPACE_ID}/environments/${process.env.CONTENTFUL_ENVIRONMENT_ID}/assets/${id}?access_token=${process.env.CONTENTFUL_ACCESS_TOKEN}`;
  const response = await axios.get(url);
  return {
    id: id,
    title: response.data.fields['title'],
    description: response.data.fields['description'],
    file: {
      url: response.data.fields['file']['url'],
      fileName: response.data.fields['file']['fileName'],
      contentType: response.data.fields['file']['contentType'],
      // details: {
      //   size: number;
      //   image: {
      //     width: number;
      //     height: number;
      //   }
      // }
    }
  };
}