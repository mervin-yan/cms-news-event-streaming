export interface Asset {
  id: string;
  title: string;
  description?: string;
  file: {
    url: string;
    details?: {
      size: number;
      image: {
        width: number;
        height: number;
      }
    };
    fileName: string;
    contentType: string;
  }
}

export interface Image {
  id: string;
  title: string;
  description?: string;
  file: {
    url: string;
    details: {
      size: number;
      image: {
        width: number;
        height: number;
      }
    };
    fileName: string;
    contentType: string;
  }
}

export interface Author {
  name: string;
  jobTitle: string;
  image: Image;
  bio: string;
  twitterHandle: string;
}

export interface Market {
  id: string;
  name: string;
  locale: string;
}

export interface Article {
  name: string;
  headline: string;
  summary: string;
  url: string;
  thumbnail: Asset;
}

export interface EditorialLineup {
  id: string;
  name: string;
  listType: string;
  market: Market;
  articles: Article[];
}

export interface Sponsor {
  name: string;
  adTag: string;
  image: Asset;
  linkText: string;
  linkUrl: string;
  linkTrackingText: string;
}