import axios from 'axios';
import { Market } from './schema';

export const fetchMarket = async (id: string): Promise<Market> => {
  const url = `https://cdn.contentful.com/spaces/${process.env.CONTENTFUL_SPACE_ID}/environments/${process.env.CONTENTFUL_ENVIRONMENT_ID}/entries/${id}?access_token=${process.env.CONTENTFUL_ACCESS_TOKEN}`;
  const response = await axios.get(url);
  return {
    id: id,
    name: response.data['fields']['name'],
    locale: response.data['fields']['locale']
  };
}