import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import * as mongoose from 'mongoose';
import { Market } from './market.schema';

export type ContentKeywordDocument = ContentKeyword & mongoose.Document;

@Schema()
export class ContentKeyword {
  @Prop({required: true, unique: true})
  name: string;

  @Prop({required: true})
  key: string;

  @Prop({ type: [{ type: mongoose.Schema.Types.ObjectId, ref: 'Market' }], required: true })
  markets: Market[];
}

export const ContentKeywordSchema = SchemaFactory.createForClass(ContentKeyword);