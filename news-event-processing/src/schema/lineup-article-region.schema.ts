import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import * as mongoose from 'mongoose';
import { BrightcoveVideo } from './video.schema';
import { NewsArticle } from './news-article.schema';
import { Region } from './region.schema';

export type LineupArticleRegionDocument = LineupArticleRegion & mongoose.Document;

@Schema({_id: false})
export class LineupArticleRegion {
  @Prop({type: mongoose.Types.ObjectId})
  _id: mongoose.ObjectId

  // @Prop({required: true, unique: true})
  @Prop({required: true})
  name: string;

  // @Prop({ type: [{ type: mongoose.Schema.Types.ObjectId, ref: 'Region' }], required: false })
  // @Prop({ type: [{ type: mongoose.Schema.Types.ObjectId }], required: false })
  region: string[];

  // @Prop({ type: mongoose.Schema.Types.ObjectId, ref: 'NewsArticle' })
  // @Prop({ type: mongoose.Schema.Types.ObjectId})
  article: string;

  // @Prop({ type: mongoose.Schema.Types.ObjectId, ref: 'BrightcoveVideo' })
  // @Prop({ type: mongoose.Schema.Types.ObjectId })
  video: string;

  @Prop() 
  alternateHeadline: string;

  @Prop([String])
  priority: string[];

  @Prop()
  revision: number;

  @Prop({type: Date})
  createdAt: Date;

  @Prop({type: Date})
  updatedAt: Date;
}

export const LineupArticleRegionSchema = SchemaFactory.createForClass(LineupArticleRegion);