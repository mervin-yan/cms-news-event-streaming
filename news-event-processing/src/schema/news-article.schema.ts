import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import * as mongoose from 'mongoose';
import { Author } from './author.schema';
import { ContentCategory } from './content-category.schema';
import { ContentKeyword } from './content-keyword.schema';
import { ContentSource } from './content-source.schema';
import { Market } from './market.schema';
import { Sponsor } from './sponsor.schema';
import { Image } from './image.schema';

export type NewsArticleDocument = NewsArticle & mongoose.Document;

@Schema({_id: false})
export class NewsArticle {
  @Prop({type: mongoose.Types.ObjectId})
  _id: mongoose.ObjectId;

  @Prop({ required: true, unique: true })
  name: string;

  @Prop({ required: true })
  headline: string;

  // @Prop({ required: true, unique: true })
  @Prop({ required: true })
  url: string;

  // @Prop({ type: mongoose.Schema.Types.ObjectId, ref: 'Author', required: true })
  // author: Author;
  @Prop()
  author: String;

  // @Prop({required: true})
  // thumbnail: Image;

  @Prop({ required: false })
  visualFlag: string;

  // @Prop({ type: [{ type: mongoose.Schema.Types.ObjectId, ref: 'ContentCategory' }], required: true })
  @Prop([String])
  contentCategories: String[];

  // @Prop({ type: [{ type: mongoose.Schema.Types.ObjectId, ref: 'ContentKeyword' }], required: true })
  // contentKeywords: ContentKeyword[];
  @Prop([String])
  contentKeywords: String[];

  // @Prop({ type: mongoose.Schema.Types.ObjectId, ref: 'Sponsor', required: false })
  // sponsor: Sponsor;
  @Prop()
  sponsor: String;

  // @Prop({ type: mongoose.Schema.Types.ObjectId, ref: 'Sponsor', required: false })
  // appleNewsSponsor: Sponsor;
  @Prop()
  appleNewsSponsor: String;

  // @Prop({ type: mongoose.Schema.Types.ObjectId, ref: 'ContentSource', required: false })
  // contentSource: ContentSource;
  @Prop()
  contentSource: String;

  // @Prop({ type: [{ type: mongoose.Schema.Types.ObjectId, ref: 'Market' }], required: true })
  // markets: Market[];
  @Prop([String])
  markets: String[];

  // @Prop({ type: [{ type: mongoose.Schema.Types.ObjectId, ref: 'NewsArticle' }], required: false })
  // relatedLinks: NewsArticle[];
  @Prop([String])
  relatedLinks: String[];

  @Prop({ required: true })
  summary: String;

  @Prop({ required: true })
  body: String;

  @Prop({ required: false, default: false })
  safeForDistribution: boolean;

  @Prop()
  revision: number;

  @Prop({type: Date})
  createdAt: Date;

  @Prop({type: Date})
  updatedAt: Date;
}

export const NewsArticleSchema = SchemaFactory.createForClass(NewsArticle);