import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import * as mongoose from 'mongoose';
import { Market } from './market.schema';

export type ContentSourceDocument = ContentSource & mongoose.Document;

@Schema()
export class ContentSource {
  @Prop()
  name: string;

  @Prop()
  adTag: string;
  
  // image: Image;

  @Prop()
  linkText: string;

  @Prop()
  linkUrl: string;

  @Prop()
  linkTrackingText: string;

  @Prop({ type: [{ type: mongoose.Schema.Types.ObjectId, ref: 'Market' }] })
  markets: Market[];
}

export const ContentSourceSchema = SchemaFactory.createForClass(ContentSource);