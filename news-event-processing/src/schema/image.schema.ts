import { Prop } from '@nestjs/mongoose';
import { Document } from 'mongoose';

export class Image extends Document {
  @Prop()
  title: string;

  @Prop()
  url: string;

  @Prop()
  fileName: string;

  @Prop()
  contentType: string;

  @Prop()
  size: number;

  @Prop()
  width: number;

  @Prop()
  height: number;
}
