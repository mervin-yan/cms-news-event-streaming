import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import { Document } from 'mongoose';

export type MarketDocument = Market & Document;

@Schema()
export class Market {
  @Prop({ required: true, unique: true })
  name: string;

  @Prop()
  url: string;

  @Prop({ required: true })
  locale: string;
}

export const MarketSchema = SchemaFactory.createForClass(Market);