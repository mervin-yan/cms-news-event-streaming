import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import { Document } from 'mongoose';

export type BrightcoveVideoDocument = BrightcoveVideo & Document;

@Schema()
export class BrightcoveVideo {
  @Prop({required: true})
  name: string;

  @Prop()
  url: string;

}

export const BrightcoveVideoSchema = SchemaFactory.createForClass(BrightcoveVideo);