import { CacheModule, Module } from '@nestjs/common';
import { MongooseModule } from '@nestjs/mongoose';
import { AppService } from './app.service';
import { ConfigModule, ConfigService } from '@nestjs/config';

import * as redisStore from 'cache-manager-redis-store';

import configuration from './config/configuration';
import { EventStoreModule } from './event-store/event-store.module';
import { ContentfulModule } from './contentful/contentful.module';
import { EditorialLineupService } from './editorial-lineup/editorial-lineup.service';
import { SponsorService } from './sponsor/sponsor.service';
import { HealthModule } from './health/health.module';
import { Author, AuthorSchema } from './schema/author.schema';
import { NewsArticle, NewsArticleSchema } from './schema/news-article.schema';
import { LineupArticleRegion, LineupArticleRegionSchema } from './schema/lineup-article-region.schema';
import { EditorialLineup, EditorialLineupSchema } from './schema/editorial-lineup.schema';

@Module({
  imports: [
    MongooseModule.forRoot('mongodb://localhost:27017/cms-news'),
    MongooseModule.forFeature([
      { name: Author.name, schema: AuthorSchema },
      { name: NewsArticle.name, schema: NewsArticleSchema },
      { name: LineupArticleRegion.name, schema: LineupArticleRegionSchema },
      { name: EditorialLineup.name, schema: EditorialLineupSchema },
    ]),
    CacheModule.register({
      store: redisStore,
      host: 'localhost',
      port: 6379,
    }),
    // CacheModule.register(),
    ConfigModule.forRoot({
      load: [configuration],
      isGlobal: true,
    }),
    EventStoreModule.forRootAsync({
      useFactory: async (config: ConfigService) => {
        return {
          connectionString: config.get('eventstore.connectionString')
        };
      },
      inject: [ConfigService]
    }),
    ContentfulModule.forRootAsync({
      useFactory: async (config: ConfigService) => {
        return {
          spaceId: config.get('contentful.spaceId'),
          accessToken: config.get('contentful.accessToken'),
          environmentId: config.get('contentful.environmentId')
        };
      },
      inject: [ConfigService]
    }),
    HealthModule,
  ],
  providers: [AppService, EditorialLineupService, SponsorService],
  controllers: [],
})
export class AppModule {}
