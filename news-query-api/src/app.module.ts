import { Module } from '@nestjs/common';
import { MongooseModule } from '@nestjs/mongoose';

import { AppController } from './app.controller';
import { AppService } from './app.service';
import { NewsListModule } from './news-list/news-list.module';

import { HealthModule } from './health/health.module';
import { TargetRegionModule } from './target-region/target-region.module';

import { ConfigModule, ConfigService } from '@nestjs/config';
import configuration from './config/configuration';
import { EditorialLineup, EditorialLineupSchema } from './schema/editorial-lineup.schema';
import { Market, MarketSchema } from './schema/market.schema';

@Module({
  imports: [
    MongooseModule.forRoot('mongodb://localhost:27017/cms-news'),

    NewsListModule,
    HealthModule,
    TargetRegionModule,
    ConfigModule.forRoot({
      load: [configuration],
      isGlobal: true,
    }),
  ],
  controllers: [AppController],
  providers: [AppService],
})
export class AppModule {}
