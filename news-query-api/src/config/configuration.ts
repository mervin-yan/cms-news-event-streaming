export default () => ({
  profileDomain: process.env.PROFILE_DOMAIN,
  profileEndpoint: process.env.PROFILE_ENDPOINT
});