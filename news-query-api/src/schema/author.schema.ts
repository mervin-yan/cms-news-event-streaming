import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import { Document, ObjectId, Types } from 'mongoose';
import { Image } from './image.schema';

export type AuthorDocument = Author & Document;

@Schema({_id: false})
export class Author {

  @Prop({type: Types.ObjectId})
  _id: ObjectId
  
  @Prop({required: true})
  name: string;

  @Prop()
  jobTitle: string;

  // @Prop()
  // image: Image;

  @Prop()
  bio: string;

  @Prop()
  twitterHandle: string;

  @Prop()
  revision: number;

  @Prop({type: Date})
  createdAt: Date;

  @Prop({type: Date})
  updatedAt: Date;
}

export const AuthorSchema = SchemaFactory.createForClass(Author);