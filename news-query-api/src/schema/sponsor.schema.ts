import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import * as mongoose from 'mongoose';
import { Market } from './market.schema';

export type SponsorDocument = Sponsor & mongoose.Document;

@Schema()
export class Sponsor {
  @Prop()
  name: string;

  @Prop()
  adTag: string;

  // image: Image;

  @Prop()
  linkText: string;

  @Prop()
  linkUrl: string;

  @Prop({ required: true })
  linkTrackingText: string;

  @Prop({ type: [{ type: mongoose.Schema.Types.ObjectId, ref: 'Market' }], required: true })
  markets: Market[];
}

export const SponsorSchema = SchemaFactory.createForClass(Sponsor);