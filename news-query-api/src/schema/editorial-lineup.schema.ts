import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import * as mongoose from 'mongoose';
import { Market } from './market.schema';

export type EditorialLineupDocument = EditorialLineup & mongoose.Document;

@Schema({_id: false})
export class EditorialLineup {
  @Prop({type: mongoose.Types.ObjectId})
  _id: mongoose.ObjectId;

  @Prop()
  name: string;

  @Prop({ required: true })
  listType: string;

  // @Prop({ type: [{ type: mongoose.Schema.Types.ObjectId, ref: 'Market' }], required: true })
  @Prop([String])
  lineupArticleRegionList: String[];

  // @Prop({ type: [{ type: mongoose.Schema.Types.ObjectId, ref: 'Market' }], required: true })
  @Prop([String])
  // markets: Market[];
  markets: String[];

  @Prop()
  revision: number;

  @Prop({type: Date})
  createdAt: Date;

  @Prop({type: Date})
  updatedAt: Date;
}

export const EditorialLineupSchema = SchemaFactory.createForClass(EditorialLineup);