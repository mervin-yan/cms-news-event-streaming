import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import { Document } from 'mongoose';

export type RegionDocument = Region & Document;

@Schema()
export class Region {
  @Prop({ required: true, unique: true })
  name: string;

  @Prop({ required: true, unique: true })
  regionId: string;
}

export const RegionSchema = SchemaFactory.createForClass(Region);