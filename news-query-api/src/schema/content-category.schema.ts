import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import * as mongoose from 'mongoose';
import { Market } from './market.schema';

export type ContentCategoryDocument = ContentCategory & mongoose.Document;

@Schema()
export class ContentCategory {
  @Prop()
  name: string;

  @Prop()
  key: string;

  @Prop({ unique: true })
  url: string;

  @Prop({ default: false })
  displayInNavigation: boolean;

  @Prop({ type: [{ type: mongoose.Schema.Types.ObjectId, ref: 'Market' }], required: true })
  markets: Market[];
}

export const ContentCategorySchema = SchemaFactory.createForClass(ContentCategory);