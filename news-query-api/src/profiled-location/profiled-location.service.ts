import { Inject, Injectable, Logger } from '@nestjs/common';
import axios from 'axios';
import * as parser from "xml2js";

@Injectable()
export class ProfiledLocationService {
  private logger: Logger = new Logger(this.constructor.name);

  
  constructor() {

  }

  public async getProfiledLocationData(placeCode: string, preview: boolean = false): Promise<string> {
    const profileUrl: string = "http://" + process.env.PROFILE_DOMAIN + process.env.PROFILE_ENDPOINT + placeCode;
    const response = await axios.get(profileUrl);

    const data = response.data;

    return data;
  }

  public extactNaadsRegions(profileXmlResponse: string): number[] {
    const dataStored: any = [];
      parser.parseString(profileXmlResponse, (err: any, result: any) => {
        for (const dataset of result.Data.CONTENT[0].LAYER) {
          if (dataset.$.name === "NAAD_WARNING") {
            for (const contentSet of dataset.CONTENT) {
              dataStored.push(contentSet.$.code);
            }

          }
        }
      });
      return dataStored.join(",");
  }
}