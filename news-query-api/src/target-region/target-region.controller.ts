import { Controller, Get, Logger, Param, Query } from '@nestjs/common';
import { TargetRegionService } from './target-region.service';

interface QueryParameter {
  placeCode?: string;
  regionCode?: string;
  regionId?: number;
}

@Controller('target-region')
export class TargetRegionController {
  private logger: Logger = new Logger(this.constructor.name);
  
  constructor(private readonly targetRegionService: TargetRegionService) {

  }
  
  @Get()
  getTargetRegionIds(@Query() query: QueryParameter): number[] {
    if (query.placeCode) {
      return this.targetRegionService.getTargetRegionIdsByPlaceCode(query.placeCode);
    }
    if (query.regionCode) {
      return this.targetRegionService.getTargetRegionIdsByRegionCode(query.regionCode);
    }
    if (query.regionId) {
      return this.targetRegionService.getTargetRegionIdsByRegionId(query.regionId);
    }
    return this.targetRegionService.getAllTargetRegionIds();
  }


}
