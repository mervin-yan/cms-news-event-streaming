import { Test, TestingModule } from '@nestjs/testing';
import { TargetRegionService } from './target-region.service';

describe('TargetRegionService', () => {
  let service: TargetRegionService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [TargetRegionService],
    }).compile();

    service = module.get<TargetRegionService>(TargetRegionService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});
