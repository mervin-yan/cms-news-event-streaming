import { Module } from '@nestjs/common';
import { TargetRegionController } from './target-region.controller';
import { TargetRegionService } from './target-region.service';


@Module({
  imports: [],
  controllers: [TargetRegionController],
  providers: [TargetRegionService]
})
export class TargetRegionModule { }
