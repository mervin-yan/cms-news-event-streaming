import { Test, TestingModule } from '@nestjs/testing';
import { TargetRegionController } from './target-region.controller';

describe('TargetRegionController', () => {
  let controller: TargetRegionController;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [TargetRegionController],
    }).compile();

    controller = module.get<TargetRegionController>(TargetRegionController);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });
});
