import { Test, TestingModule } from '@nestjs/testing';
import { NewsListService } from './news-list.service';

describe('NewsListService', () => {
  let service: NewsListService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [NewsListService],
    }).compile();

    service = module.get<NewsListService>(NewsListService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});
