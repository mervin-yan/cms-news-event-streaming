import { CACHE_MANAGER, Inject, Injectable, Logger } from '@nestjs/common';

import { Cache } from 'cache-manager';

import { TargetRegionService } from '../target-region/target-region.service';

import { Model } from 'mongoose';
import { InjectModel } from '@nestjs/mongoose';
// import { LineupArticleRegion, LineupArticleRegionDocument } from './schema/lineup-article-region.schema';
// import { NewsArticle, NewsArticleDocument } from './schema/news-article.schema';
// import { Author, AuthorDocument } from './schema/author.schema';
import { Market, MarketDocument } from '../schema/market.schema';
import { EditorialLineup as EditorialLineup1, EditorialLineupDocument } from '../schema/editorial-lineup.schema';

@Injectable()
export class NewsListService {
  private logger: Logger = new Logger(this.constructor.name);

  constructor(@Inject(CACHE_MANAGER) private cacheManager: Cache, private regionService: TargetRegionService, @InjectModel(EditorialLineup1.name) private editorialLineupModel: Model<EditorialLineupDocument>, @InjectModel(Market.name) private marketModel: Model<MarketDocument>) {

  }

  async getNewsList(locale: string, listType: string, quantity: number = 10) {
    const key = `${locale.toLocaleLowerCase()}-${listType.toLocaleLowerCase()}`;
    const cachedItem = await this.cacheManager.get(key);
    this.logger.log(cachedItem);
    return cachedItem;
  }

  async getTrendingNowWithLatest(locale: string, listType: string, placeCode: string, preview: boolean = false, quantity: number = 12) {

  }

  async getEditorialLineup(locale: string, listType: string, placeCode: string, preview: boolean = false, priority: string = "", regionId: number = 0, doVideoResolution: boolean = true) {
    //get maketId based on locale and status 'publish'
    const market = await this.marketModel.findOne(
      {
        locale: locale,
        status: 'publish'
      }
    );

    //get editorial-lineup by marketId and listType

    const editorialLineup = await this.editorialLineupModel.findOne(
      {
        markets: market._id,
        listType: listType,
      }
    );

    let placeCodeRegions: number[] = [];
    let trim: boolean = false;
    if (placeCode && placeCode !== '-') {
      placeCodeRegions = await this.regionService.getTargetRegionIdsByPlaceCode(placeCode);
      trim = true;
    } else if (regionId) {
      placeCodeRegions = await this.regionService.getTargetRegionIdsByRegionId(regionId);
    }

    //filter editorial lineup items by regionIds
    //lineup item's region contains at least one of the regionIds

    //filter editorial lineup items by priority
    //either lineup item has no priority or its priority matches the priortity parameter

    //listType is headlineText
    //for each of lineup item which has bc_url, transform it to new object
    //message: lineupItem.headline, CTA: lineupItem.url, priority: lineupItem.priority


  }
}
