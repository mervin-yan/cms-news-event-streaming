import { Controller, Get, Logger, Param, Query } from '@nestjs/common';
import { NewsListService } from './news-list.service';

interface QueryParameter {
  preview?: boolean;
  priority?: string;
  thirdParty?: boolean;
  resolveVideo?: boolean;
  regionId?: number;
}

@Controller('news-list')
export class NewsListController {

  private logger: Logger = new Logger(this.constructor.name);
  
  constructor(private readonly newsListService: NewsListService) {

  }
  
  @Get('/:locale/line-up-region-with-latest/:listType/:quantity')
  getLineupRegionWithLatest(@Param('locale') locale: string, @Param('listType') listType: string, @Param('quantity') quantity: number): any {
    this.logger.log(`loale: ${locale}`);
    this.logger.log('listType: ${listType}');
    return this.newsListService.getNewsList(locale, listType, quantity);
  }

  @Get('/:locale/line-up/:listType/:placeCode')
  getLineup(@Param('locale') locale: string, @Param('listType') listType: string, @Param('placeCode') placeCode: string, @Query() query: QueryParameter): any {
    this.logger.log(`loale: ${locale}`);
    this.logger.log('listType: ${listType}');

    const preview = query.preview;
    const priority = query.priority;

    if (listType === 'trendingNow') {
      return this.newsListService.getTrendingNowWithLatest(locale, listType, placeCode, preview);
    } else {
      return this.newsListService.getEditorialLineup(locale, listType, placeCode, preview, priority);
    }
    
  }
}

// thirdparty=true&resolvevideos=false&regioncode=1025