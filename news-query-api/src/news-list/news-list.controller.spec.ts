import { Test, TestingModule } from '@nestjs/testing';
import { NewsListController } from './news-list.controller';

describe('NewsListController', () => {
  let controller: NewsListController;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [NewsListController],
    }).compile();

    controller = module.get<NewsListController>(NewsListController);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });
});
