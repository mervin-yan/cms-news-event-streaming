import { CacheModule, Module } from '@nestjs/common';
import { NewsListController } from './news-list.controller';
import { NewsListService } from './news-list.service';
import * as redisStore from 'cache-manager-redis-store';
import { TargetRegionService } from 'src/target-region/target-region.service';
import { EditorialLineup, EditorialLineupSchema } from 'src/schema/editorial-lineup.schema';
import { Market, MarketSchema } from 'src/schema/market.schema';
import { MongooseModule } from '@nestjs/mongoose';

@Module({
  imports: [
    CacheModule.register({
      store: redisStore,
      host: 'localhost',
      port: 6379,
    }),
    MongooseModule.forFeature([
      // { name: Author.name, schema: AuthorSchema },
      // { name: NewsArticle.name, schema: NewsArticleSchema },
      // { name: LineupArticleRegion.name, schema: LineupArticleRegionSchema },
      { name: EditorialLineup.name, schema: EditorialLineupSchema },
      { name: Market.name, schema: MarketSchema },
    ]),
  ],
  controllers: [NewsListController],
  providers: [NewsListService, TargetRegionService]
})
export class NewsListModule { }
